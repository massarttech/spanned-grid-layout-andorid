package com.massarttech.android.spannedgridlayout.demo


import java.io.Serializable

data class Data(var image:String,var isBig:Boolean=false) : Serializable
