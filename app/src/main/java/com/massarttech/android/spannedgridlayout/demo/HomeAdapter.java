package com.massarttech.android.spannedgridlayout.demo;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.massarttech.android.spannedgridlayoutmanager.SpanLayoutParams;
import com.massarttech.android.spannedgridlayoutmanager.SpanSize;


import java.util.List;

class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> {
    private final Context context;
    private List<Data> data;

    public HomeAdapter(List<Data> data, Context context) {
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_home_recycler, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Data item = data.get(position);
        if (item.isBig()) {
            View view=new View(context);
            view.setLayoutParams(new SpanLayoutParams(new SpanSize(2,2)));
            holder.itemView.setLayoutParams(new SpanLayoutParams(new SpanSize(2, 2)));
            holder.imageView.getLayoutParams().height = holder.imageView.getLayoutParams().height * 2;
            Glide.with(context)
                    .load(item.getImage())
                    .placeholder(new ColorDrawable(MaterialColorPalette.getRandomColor("500")))
                    .into(holder.imageView);
        }  else {
            holder.itemView.setLayoutParams(new SpanLayoutParams(new SpanSize(1, 1)));
            Glide.with(context)
                    .load(item.getImage())
                    .placeholder(new ColorDrawable(MaterialColorPalette.getRandomColor("500")))
                    .into(holder.imageView);
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;

        ViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.himageview);
        }
    }
}