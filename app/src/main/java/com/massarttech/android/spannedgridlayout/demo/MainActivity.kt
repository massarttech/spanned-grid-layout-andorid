package com.massarttech.android.spannedgridlayout.demo

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.massarttech.android.spannedgridlayoutmanager.SpannedGridLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val data=buildData()
        val layoutManager = SpannedGridLayoutManager(
            SpannedGridLayoutManager.Orientation.VERTICAL, 3
        )
        rvHome.layoutManager = layoutManager
        val adapter =HomeAdapter(data, this)
        rvHome.adapter=adapter
    }

    private fun buildData():List<Data>{
        val list= mutableListOf<Data>()
        list.add(Data("https://cdn.pixabay.com/photo/2015/01/28/23/10/mosque-615415_960_720.jpg"))
        list.add(Data("https://cdn.pixabay.com/photo/2015/02/19/12/24/mosque-642019_960_720.jpg"))
        list.add(Data("https://cdn.pixabay.com/photo/2015/08/31/07/30/aya-sofia-915076_960_720.jpg"))
        list.add(Data("https://cdn.pixabay.com/photo/2016/06/27/22/18/moscow-cathedral-mosque-1483524_960_720.jpg",true))
        list.add(Data("https://cdn.pixabay.com/photo/2015/02/20/00/48/mosque-642849_960_720.jpg"))
        list.add(Data("https://cdn.pixabay.com/photo/2015/01/31/08/49/mosque-618303_960_720.jpg"))
        list.add(Data("https://cdn.pixabay.com/photo/2013/10/17/14/03/bibi-xanom-196898_960_720.jpg"))
        list.add(Data("https://cdn.pixabay.com/photo/2015/01/28/23/10/mosque-615415_960_720.jpg"))
        list.add(Data("https://cdn.pixabay.com/photo/2015/02/19/12/24/mosque-642019_960_720.jpg"))
        list.add(Data("https://cdn.pixabay.com/photo/2015/08/31/07/30/aya-sofia-915076_960_720.jpg"))
        list.add(Data("https://cdn.pixabay.com/photo/2016/06/27/22/18/moscow-cathedral-mosque-1483524_960_720.jpg"))
        list.add(Data("https://cdn.pixabay.com/photo/2015/02/20/00/48/mosque-642849_960_720.jpg"))
        list.add(Data("https://cdn.pixabay.com/photo/2015/01/31/08/49/mosque-618303_960_720.jpg"))
        list.add(Data("https://cdn.pixabay.com/photo/2013/10/17/14/03/bibi-xanom-196898_960_720.jpg"))
        return list
    }
}
