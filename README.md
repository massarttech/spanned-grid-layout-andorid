# Spanned Layout Manager Android [![](https://jitpack.io/v/com.gitlab.massarttech/spanned-grid-layout-andorid.svg)](https://jitpack.io/#com.gitlab.massarttech/spanned-grid-layout-andorid)


**Layout manager to use with `RecyclerView` that looks like `GridLayoutManager` but with spans**
<br/>
<img src="https://gitlab.com/massarttech/spanned-grid-layout-andorid/raw/master/screens/screen1.png?inline=false" width="300" height="500"/>


## How To Use

### Setup
Add it to your build.gradle with:
```gradle
allprojects {
    repositories {
        maven { url "https://jitpack.io" }
    }
}
```
and:

```gradle
dependencies {
     implementation 'com.gitlab.massarttech:spanned-grid-layout-android:1.0.0'
}
```
**For project not using `AndroidX`**
```gradle
dependencies{
	  implementation 'com.gitlab.massarttech:spanned-grid-layout-andorid:2.0.0'
}

```

### Usage
#### Set `SpannedLayoutManager` to `RecyclerView`
```kotlin
 val layoutManager = SpannedGridLayoutManager(
             SpannedGridLayoutManager.Orientation.VERTICAL, 3
         )
         rvHome.layoutManager = layoutManager
```
#### In `onBindViewHolder(...)` method of adapter set spans
```java
 Data item = data.get(position);
        if (item.isBig()) {
            holder.itemView.setLayoutParams(new SpanLayoutParams(new SpanSize(2, 2)));
            holder.imageView.getLayoutParams().height = holder.imageView.getLayoutParams().height * 2;
            Glide.with(context)
                    .load(item.getImage())
                    .placeholder(new ColorDrawable(MaterialColorPalette.getRandomColor("500")))
                    .into(holder.imageView);
        }  else {
            holder.itemView.setLayoutParams(new SpanLayoutParams(new SpanSize(1, 1)));
            Glide.with(context)
                    .load(item.getImage())
                    .placeholder(new ColorDrawable(MaterialColorPalette.getRandomColor("500")))
                    .into(holder.imageView);
        }
```